# Timeless

An attempt to just, not know the time.

But first I had to prepare, I did this by:
- Covering clocks around house (Easy)
    - Digital ones unplugged, batteries taken out of analogue 
- Windows 10 Clock (Easy)
    - Right click taskbar, "Disable Taskbar Icons", Clock off..
 
 ## Timestamps on various platforms (Hard)
 
 This step takes preparing and a lot of time, for this I used CSS and uBlock rules.

uBlock rules
```
! tweet timestamps, changes all the time :/
twitter.com##a.r-qvutc0.r-bcqeeo.r-ad9z0x.r-1qd0xha.r-1loqt21.r-111h2gw.css-16my406.css-901oao.css-18t94o4.css-4rbku5 > .r-qvutc0.r-bcqeeo.r-ad9z0x.r-1qd0xha.css-16my406.css-901oao
! dm timestamps, also change
twitter.com##.r-qvutc0.r-fdjqy7.r-bcqeeo.r-1sf4r6n.r-16dba41.r-n6v787.r-1qd0xha.r-111h2gw.css-901oao
twitter.com##.css-1dbjc4n.r-1awozwy.r-11u3ssh.r-1jkafct.r-7q8q6z.r-xoduu5.r-z80fyv.r-1777fci.r-ppnd1t.r-ou255f.r-633pao.r-u8s1d.r-1jaylin
```
Discord css
```css
[class^="timestamp-"] {
    display: none;
}
[class^="latin12CompactTimeStamp-"] {
    display: none;
}
[class^="embedFooterText-"] {
    display: none;
}
```
The Lounge css
```css
[class^="time tooltipped tooltipped-e"] {
    display: none;
}
```

# Results

After about a week, I'm pretty disorientated about time passing, I may choose to continue